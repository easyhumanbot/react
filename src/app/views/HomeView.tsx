import * as React from 'react';
import { Link } from 'react-router';

var HomeView2 = React.createClass({
    getInitialState: function() {
        return { loaded: false };
    },
    
    componentDidMount: function() {
        this.setState({ loaded: true });
    },
    
    render: function() {
        var loading = this.state.loaded ? "" : " (loading...)";
        return <div>
            <h2>HomeView {loading}</h2>
            <div><Link to="/about">About</Link></div>
        </div>;
    }
});

var colors = {
	NICE: 'pink',
	SUPER_NICE: 'SUPER_NICE'
}

export default class HomeView extends React.Component<any, any> {
    render() {
        return <div>
                <Counter increment={1} color={colors.NICE} />
                <Counter increment={5} color={colors.SUPER_NICE} />
		    </div>;
    }
}


interface ICounterProps {
	increment: number;
	color: string;
}

interface ICounterState {
	counter: number;
}

class Counter extends React.Component<ICounterProps, ICounterState> {
	private interval: number;

	constructor(props) {
		super(props);
		this.state = { counter: 0 };
		this.interval = window.setInterval(() => this.tick(), 1000);
	}

	tick() {
		this.setState({
			counter: this.state.counter + this.props.increment
		}); 
	}

	componentWillUnmount() {
		window.clearInterval(this.interval);
	}

	render() {
		return (
			<h1 style={{ color: this.props.color }}>
				Counter ({this.props.increment}): {this.state.counter}
			</h1>
		);
	}
}