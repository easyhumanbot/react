import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Router, hashHistory } from 'react-router';
import routes from './app/routes/routes';

import { IStore, createStore } from 'redux';
import { Provider } from 'react-redux';

class App extends React.Component<{}, {}> {
	render() {
		return (
			<Router history={hashHistory}>
				{routes}
			</Router>
		);
	}
}

ReactDOM.render(<App />, document.getElementById('root'));
